\contentsline {chapter}{Abstract}{1}{Doc-Start}
\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}Related work}{4}{chapter.2}
\contentsline {chapter}{\numberline {3}Problem setting for work performance prediction}{6}{chapter.3}
\contentsline {section}{\numberline {3.1}Linear regression based work performance prediction}{6}{section.3.1}
\contentsline {section}{\numberline {3.2}Limitation of the baseline model}{6}{section.3.2}
\contentsline {chapter}{\numberline {4}Designing feature descriptors}{8}{chapter.4}
\contentsline {section}{\numberline {4.1}Movement features}{8}{section.4.1}
\contentsline {section}{\numberline {4.2}Location and Colocation features}{8}{section.4.2}
\contentsline {section}{\numberline {4.3}Face to face communication features}{10}{section.4.3}
\contentsline {chapter}{\numberline {5}Multitask KPI regression with group lasso}{11}{chapter.5}
\contentsline {chapter}{\numberline {6}Experiment Result}{13}{chapter.6}
\contentsline {section}{\numberline {6.1}Motivation of the Experiment}{13}{section.6.1}
\contentsline {section}{\numberline {6.2}Raw sensor and KPI Dataset}{13}{section.6.2}
\contentsline {section}{\numberline {6.3}Feature descriptors for comparison}{14}{section.6.3}
\contentsline {section}{\numberline {6.4}Regression models for comparison}{18}{section.6.4}
\contentsline {section}{\numberline {6.5}Evaluation metric}{18}{section.6.5}
\contentsline {section}{\numberline {6.6}Result and discussion}{18}{section.6.6}
\contentsline {subsection}{\numberline {6.6.1}Visualization of communication dataset}{19}{subsection.6.6.1}
\contentsline {subsection}{\numberline {6.6.2}Result comparison for multi-modal communication features}{22}{subsection.6.6.2}
\contentsline {subsection}{\numberline {6.6.3}Result comparison for multitask KPI prediction}{23}{subsection.6.6.3}
\contentsline {chapter}{\numberline {7}Conclusion}{25}{chapter.7}
\contentsline {chapter}{Acknowledgement}{27}{chapter*.2}
\contentsline {chapter}{References}{29}{chapter*.3}
